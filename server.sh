#!/usr/bin/env bash
set -e

touch "server.log"

echo "    Starting the PHP builtin webserver"
php -S 127.0.0.1:8080 -t "testapp/" > /dev/null 2> "server.log" &
