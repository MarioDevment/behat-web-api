# WebApiExtension

[![pipeline status](https://gitlab.com/MarioDevment/behat-web-api/badges/master/pipeline.svg)](https://gitlab.com/MarioDevment/behat-web-api/commits/master)
Provides testing for JSON APIs with Behat 3

## Documentation

[Official documentation](doc/index.rst)

## Copyright

Copyright (c) 2014 Konstantin Kudryashov (ever.zet). See LICENSE for details.

## Contributors

* Christophe Coevoet [stof](http://github.com/stof) [lead developer]
* Other [awesome developers](https://github.com/Behat/WebApiExtension/graphs/contributors)
